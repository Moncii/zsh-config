# Clone zcomet if necessary
if [[ ! -f ${ZDOTDIR:-${HOME}}/.zcomet/bin/zcomet.zsh ]]; then
  command git clone https://github.com/agkozak/zcomet.git ${ZDOTDIR:-${HOME}}/.zcomet/bin
fi

source ${ZDOTDIR:-${HOME}}/.zcomet/bin/zcomet.zsh

zcomet load "zsh-users/zsh-syntax-highlighting"
zcomet load "zsh-users/zsh-autosuggestions"
zcomet load "zsh-users/zsh-history-substring-search"
zcomet load "sindresorhus/pure"

zcomet compinit

alias ls="exa"
alias cat="bat"
alias dev="docker start -i dev"

#alias sudo="doas"
#alias sudoedit"doas nvim"

zstyle ':completion:*' menu select

export PF_ASCII="linux"
export PF_INFO="ascii title os host kernel uptime pkgs shell memory"
echo ""
pfetch
date

autoload -U promptinit; promptinit
prompt pure
